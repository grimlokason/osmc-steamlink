#!/bin/bash

LAUNCHER_PATH=$( dirname "${BASH_SOURCE[0]}" )

echo ${LAUNCHER_PATH}
sudo su osmc -c "bash ${LAUNCHER_PATH}/heartbeat.sh &" &
#sudo vcgencmd hdmi_channel_map 0x0BFac4c8
sudo su osmc -c "openvt -c 7 -s -f /usr/bin/steamlink & sudo /opt/vc/bin/vcgencmd hdmi_channel_map 0x0BFac4c8" &
sudo openvt -c 7 -s -f clear

sleep 2

sudo su -c "systemctl stop mediacenter &" &

exit
