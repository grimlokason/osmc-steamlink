1st version tested on kodi 18 with OSMC  
needed : steamlink from here : https://steamcommunity.com/app/353380/discussions/0/1743353164093954254/


# 5.1 surround on osmc for steamlink

You'll need to login as osmc (shell or ssh, as you wish)

## install the latest available kernel : 

You need to get the latest kernel wich include some sound drivers patch 

* Add the dev repo

```sh
if ! grep -Hri "deb http://apt.osmc.tv stretch-devel main" /etc/apt/sources.list*
    then 
    sudo echo "deb http://apt.osmc.tv stretch-devel main" > /etc/apt/sources.list.d/stretch-devel-apt
    else 
    echo "nothing to do"
fi
```

* Upgrade your system

```sh
sudo apt-get update
sudo apt-get dist-upgrade
```

## Personnalise your alsa setup

* Tell your system that you have a 5.1 sound system on hdmi

```sh
sudo vcgencmd hdmi_channel_map 0x0BFac4c8
```

* Redirect everything that is send to the inexistant sound card "pcm.sourround51"
by àddinf the following to the ~/.asoundrc file 

```
pcm.!surround51 {
    type route
    slave.pcm "hw:0,1"
    slave.channels 6
  ttable {
    0.0= 1
    1.1= 1
    2.4= 1
    3.5= 1
    4.2= 1
    5.3= 1
  }
}
```

Please note that the ttable mapping has been done on my sound system.
you need to use speaker-test to check if it's ok for you.

* sound test

```sh
speaker-test -c6 -Dsurround51
```

If the mapping is not ok, you'll need to change the mapping in ~/.asoundrc
For each channel : 
* The first number correspond to that 
 0 - Front Left  
 4 - Center  
 1 - Front Right  
 3 - Rear Right  
 2 - Rear Left  
 5 - LFE  

To test channel by channel: 

```sh
speaker-test -c6 -Dsurround51 -sX
```

where x is a number from 1 to 6

## testing time !

### launch from osmc

* Install that one plugin  
** On gitlab : downlaod as a zip  
** On kodi : install from zip

* Use the freshly installed plugin to launch steamlink  
( NB : other launcher are not going to work because they do not change the hdmi_channel_map before launching steamlink / kodi reset it at launch :( )

* Setup your steamlink to use surround51, launch a game and Enjoy ! ( I hope ^^' )